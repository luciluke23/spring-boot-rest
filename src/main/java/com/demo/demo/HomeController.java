package com.demo.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ankit.dao.Student;

@RestController
public class HomeController {

	@RequestMapping("data")
	public List<Student> home() 
	{

		List<Student> list = new ArrayList<Student>();

		Student s1 = new Student();
		s1.setRollNo(25);
		s1.setName("Ankit");

		Student s2 = new Student();
		s2.setRollNo(26);
		s2.setName("Alien");

		list.add(s1);
		list.add(s2);

		return list;
	}

}
